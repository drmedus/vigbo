import React, { Component } from 'react';
import FindACoupleGame from "./components/FindACoupleGame";

class App extends Component {
  render() {
    return (
      <FindACoupleGame />
    );
  }
}

export default App;
