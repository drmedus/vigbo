import React, { PureComponent } from 'react';
import SeparateNumber from "./SeparateNumber";
import { Container, Row, Col } from 'react-bootstrap';

export default class FindACoupleGame extends PureComponent {
  constructor() {
    super();

    this.memoryTime = 5000;  // time to remember numbers
    this.errorTime = 1000;

    this.state = {
      items: [], // couples of numbers array (element value)
      visibleItems: [], // visible elements (element id)
      checkedCouple: [] // a couple of elements to compare (element id)
    };

    this.hideCouple = this.hideCouple.bind(this);
    this.hideAllItems = this.hideAllItems.bind(this);

    //Item generation
    this.lineLength = 4; //square 4*4
    this.numbers = Array.from(
        {length: 8},
        () => Math.floor(Math.random() * 500)
    ); //generating an array of random numbers

    this.numbers.push(...this.numbers); // we need couples of numbers

    this.numbers.sort(function () { // random sorting
      return Math.random() - 0.5;
    });
  }

  componentDidMount() { // game init
    this.setState({
      items: [...this.numbers],
      visibleItems: Array.from({ length: 16 }, (v, k) => k), // show all elements
    }, this.hideAllItems); // callback to hide all elements
  }

  isVisible(itemId) {
    return this.state.visibleItems.indexOf(itemId) > -1 || this.state.checkedCouple.indexOf(itemId) > -1;
  } // Logic flag for separate number

  handleClick(itemId, itemNumber) {
    this.setState({
      checkedCouple: [itemId].concat(this.state.checkedCouple)
    }, this.checkCouple);

  }

  hideCouple() {
    this.setState({ checkedCouple: [] });
  }

  hideAllItems() {
    setTimeout(() => this.setState({ visibleItems: [] }), this.memoryTime);
  }

  checkCouple() {
    if (this.state.checkedCouple.length > 1) {
      if (this.state.items[this.state.checkedCouple[0]] === this.state.items[this.state.checkedCouple[1]]) {
        this.setState((state) => {
              state.visibleItems.push(this.state.checkedCouple[0], this.state.checkedCouple[1]);
              return {
                visibleItems: [...state.visibleItems],
                checkedCouple: []
              }
            }
        )
      } else {
        setTimeout(this.hideCouple, this.errorTime);
      }
    }

  }

  generateField = () => {
    let table = [];
    let shift = 0;
    let itemId = 0;

    for (let i = 0; i < 4; i++) {
      let children = []

      for (let j = 0; j < 4; j++) {

        children.push(
            <Col key={itemId} onClick={this.handleClick.bind(this, itemId, this.state.items[j + shift])}>
              <SeparateNumber
                  id={this.state.items[j + shift]}
                  isVisible={this.isVisible(itemId)}
                  number={this.state.items[j + shift]}
              />
            </Col>
        )
        itemId++;
      }
      table.push(<Row key={i}>{children}</Row>)
      shift = shift + 4;
    }

    return table
  }

  render() {
    return (
        <div className="game-container">
          <Container>
            {this.generateField()}
          </Container>;
        </div>
    )
  }

}
