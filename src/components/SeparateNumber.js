import React, { PureComponent } from 'react';

export default class SeparateNumber extends PureComponent {
  get className() {
    return this.props.isVisible ? 'item visible' : 'item';
  }

  render() {
    return (
        <div className={this.className}>
          <div className="rotator">
            <div className="front-side">
              ???
            </div>
            <div className="back-side">
              {this.props.number}
            </div>
          </div>
        </div>
    );
  }
}
